# USA.state.boundaries.data

R data package containing 3 sets of maps of the United States of America (USA):

1) A map of the USA with the NAD 1983 Albers Projection from the USGS that was originally part of `USGSstates2k` which has been archived and is no longer maintained. This map is identified as USA_state_boundaries. Irucka worked with that data set while a Cherokee Nation Technology Solutions (CNTS) USGS Contractor and/or USGS employee.

2) A NAD83 datum map of the USA, which includes all Commonwealth and State boundaries & also includes Puerto Rico and the U.S. Virgin Islands. This map is identified as statesp010. This map comes from the USGS National Map which has been discontinued.

3) A WGS84 datum map of the USA, which includes all Commonwealth and State boundaries & also includes Puerto Rico and the U.S. Virgin Islands. This map has been identified as state_boundaries_wgs84. This map is a reprojection of the NAD83 datum map from the USGS National Map.



# Installation from Irucka's drat repository

```R
if (!requireNamespace("drat", quietly = TRUE)) {

install.packages("drat")

library("drat")

} else {

library("drat")

}

addRepo("iembry", "https://iembry.gitlab.io/drat/")
# adds the iembry drat repository (source of the R package)

install.packages("USA.state.boundaries.data", repos = "https://iembry.gitlab.io/drat/", type = "source")
```



# Package Contents

This package contains three sets of map datasets:

* `USA_state_boundaries_map`: USA_state_boundaries map with `sf` read data
* `USA_state_boundaries_summary`: USA_state_boundaries Spatial summary
* `statesp010_map`: statesp010 map with `sf` read data
* `statesp010_summary`: statesp010 Spatial summary
* `state_boundaries_wgs84`: state_boundaries_wgs84 map with `sf` read data




# Examples

```R

# You can view the USA.state.boundaries: examples vignette in the
# `USA.state.boundaries` package to see the full results of the code below:


## Map 1

# to install the USA.state.boundaries.data package & load it
drat::addRepo("iembry", "https://iembry.gitlab.io/drat/")

install.packages("USA.state.boundaries.data", repos = "https://iembry.gitlab.io/drat/", type = "source")

install.load::load_package("USA.state.boundaries.data", "sf", "ggplot2")


# load the map
data(USA_state_boundaries_map)


# get the CRS information
st_crs(USA_state_boundaries_map)


# plotting with ggplot2
ggplot(USA_state_boundaries_map) + geom_sf()




## Map 2

# to install the USA.state.boundaries.data package & load it
drat::addRepo("iembry", "https://iembry.gitlab.io/drat/")

install.packages("USA.state.boundaries.data", repos = "https://iembry.gitlab.io/drat/", type = "source")

install.load::load_package("USA.state.boundaries.data", "sf", "ggplot2")


# load the map
data(statesp010_map)


# get the CRS information
st_crs(statesp010_map)


# plotting with ggplot2
ggplot(statesp010_map) + geom_sf()




## Map 3

# to install the USA.state.boundaries.data package & load it
drat::addRepo("iembry", "https://iembry.gitlab.io/drat/")

install.packages("USA.state.boundaries.data", repos = "https://iembry.gitlab.io/drat/", type = "source")

install.load::load_package("USA.state.boundaries.data", "sf", "ggplot2")


# load the map
data(state_boundaries_wgs84)


# get the CRS information
st_crs(state_boundaries_wgs84)


# plotting with ggplot2
ggplot(state_boundaries_wgs84) + geom_sf()
```



# Disclaimer

This software is in the public domain because it contains materials that originally came from the U.S. Geological Survey (USGS), an agency of the United States (US) Department of Interior (DOI). For more information, see the official [USGS copyright policy](https://www.usgs.gov/information-policies-and-instructions/copyrights-and-credits#copyright)

Although parts of this software program have been used by the U.S. Geological Survey (USGS), no warranty, expressed or implied, is made by the USGS or the U.S. Government as to the accuracy and functioning of the program and related program material nor shall the fact of distribution constitute any such warranty, and no responsibility is assumed by the USGS in connection therewith.

This software is provided "AS IS."
