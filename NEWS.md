# USA.state.boundaries.data 1.0.5 (9 February 2023)

* The package vignette originally created for the `USA.state.boundaries` package has been revised to be contained in this package to help reduce the size of the `USA.state.boundaries` package so that it can be in CRAN


# USA.state.boundaries.data 1.0.4 (6 January 2023)

* Removed `USA.state.boundaries`, `graphics`, `knitr`, `rmarkdown`, `raster`, `rgdal` (being retired in 2023), and `ggspatial` from Suggests & `drat` and `sp` from Imports
* Added `spelling` and `sf` & moved `drat` to Suggests
* The creation of the data sets has been moved from `sp` and `rgdal` to `sf` only


# USA.state.boundaries.data 1.0.3 (18 January 2022)

* Removed the | file LICENSE statement from the DESCRIPTION file as it was not needed
* Removed the LICENSE file
* Made revisions to the README and DESCRIPTION files
* Third attempt to release this package in conjunction with `USA.state.boundaries`


# USA.state.boundaries.data 1.0.2 (12 January 2022)

* Made revisions to the README and DESCRIPTION files
* Added `sp` as an Imported package


# USA.state.boundaries.data 1.0.1 (11 January 2022)

* Second attempt to release this package in conjunction with `USA.state.boundaries`
* Minor revisions in the documentation


# USA.state.boundaries.data 1.0.0 (3 June 2020)

* Initial release (replacement of `USGSstates2k`) in conjunction with `USA.state.boundaries`. 
